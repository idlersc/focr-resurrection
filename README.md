# FOCR Resurrection
FOCR Resurrection is a OCR for Runescape Classic sleep CAPTCHAs. This project took the FOCR source code and implements it as a command line sleeper. It is compatible with both Windows and Linux.

This project does not claim ownership of FOCR, nor do any of the contributers completely understand how FOCR works.

# Windows Download
	https://gitlab.com/idlersc/focr-resurrection/-/tags/windows-release


# Usage
	Windows:
		Extract FOCR Resurrection to a directory. 
		Modify run.bat
		Run

	Linux:
		Clone FOCR Resurrection.
		Compile using `make` (instructions below)
		Modify run.sh
		Run

# Compilation Instructions
	Linux:
		Ensure "make" and "g++" are installed.
		Run `make`

	Windows:
		Ensure "make" and "g++" are installed via Cygwin.
		Run `make` in project directory with Cygwin Terminal.
		You will have to run focr.exe with the Cygwin DLLs in your path to run focr.exe outside of Cygwin Terminal (i.e. include c:\cygwin64\bin in your path.)

		Alternatively, you can import the files one by one into a new VS project but this is unsupported.
	
