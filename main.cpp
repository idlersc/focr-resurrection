#include "Segmentor.h"
#include "ImageClasifier.h"
#include "DictSearch.h"
#include "errordefns.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#include <cstring>
#include <fstream>
#include <iostream>
using namespace std;

Image image;
ImageClassifier imClass(image);
Segmentor segmentor;

DictSearch ds;

float *probs[256]; 



void loadDictionary(const char* dict)
{
	if(access(dict, F_OK) != 0) {
		cout << "dictionary file does not exist. Please ensure that your dictionary word list is placed at " << dict << endl;
		cout << "if you do not have the dictionary please visit the FOCR Resurrection repository at: https://gitlab.com/idlersc/focr-resurrection" << endl;
		exit(1);
	}

	ds.initialize(dict);

	int i;

	for (i = 0; i < 256; i++)
	{
		probs[i] = new float[256];
	}

}

void loadModel(const char* filename)
{
	if(access(filename, F_OK) != 0) {
		cout << "model file does not exist. Please ensure that your model file is placed at " << filename << endl;
		cout << "if you do not have the model please visit the FOCR Resurrection repository at: https://gitlab.com/idlersc/focr-resurrection" << endl;
		exit(1);
	}

	imClass.loadModel(filename);
}

void checkError(int errorCode) {
        switch(errorCode) {
		case SUCCESS:
		      break;
		case FILE_NOT_FOUND:
		      cout << "bitmap file not found!" << endl;
		      exit(1);
		case NOT_VALID_BITMAP:
		      cout << "bitmap file is not a valid bitmap file, because it did not contain the bitmap magic number inside the header." << endl;
		      exit(1);
		case NOT_1BPP:
		      cout << "bitmap file is not 1 bit per pixel (\'monochrome\'). Please convert." << endl;
		      exit(1);
		case NOT_ENOUGH_MEMORY:
		      cout << "could not reserve enough memory for execution." << endl;
		      exit(1);
        }

}


void getString(const char* imageFilename, char* result)
{

	checkError(image.load(imageFilename));
	segmentor.process(image);

	result[0] = '\0';

	if (segmentor.getNrOfLetters() == 0) return;

	char* strng = new char[segmentor.getNrOfLetters() + 1];

	int i;
	for (i = 0; i < segmentor.getNrOfLetters(); i++)
	{
		image = segmentor.getLetter(i, true);
		image.center(1);
		image.scale(15, 15);
		ImageClassifier ic(image);

		strng[i] = ic.getCharacter(probs[i]);
	}

	strng[i] = '\0';


		int k, t;
		for (k = 0; k < strlen(strng); k++)
		{
			float sum = 0.f;
			for (t = 0; t < 256; t++)
			{
				sum += probs[k][t];//probs[k][t] = 1 - probs[k][t];
			}

			for (t = 0; t < 256; t++)
			{
				probs[k][t] = 1 - probs[k][t] / sum;
			}
		}

		
		char* res = ds.getMatch(strng, probs);

		delete strng;

		strcpy(result, res);
		delete res;

//	return result;
}

void getTime(char * buffer, int bufferLength) {
	time_t rawtime;
	struct tm * timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	memset(buffer, 0, bufferLength);
	snprintf(buffer, bufferLength, "%s", asctime(timeinfo));
	strtok(buffer, "\n"); //strip newline
}

int writeToFile(char * filePath, char * data) {
	FILE *fp = fopen(filePath, "w+"); //create if not already created, truncate and start cursor from beginning.

	if(fp == NULL)
		return -1;

	fputs(data, fp);
	fclose(fp);
	return 1;
}



int main(int argc, char* argv[]) {
	char* result = new char[64];

	if(argc == 1) {
		cout << "Usage: " << argv[0] << " [FILE.bmp OR directory]" << endl;
		cout << "Resolve RSC fatigue CAPTCHAs to plaintext." << endl;
		cout << endl;
		cout << "With no input, this help message is displayed." << endl;
		cout << "If specifying a bitmap file, file.bmp must be a 1bpp bitmap image. Images which do not follow this format will result in error." << endl;
		cout << endl;
		cout << "If specifying a directory, hc.bmp must be present and must be a 1bpp bitmap image. Images which do not follow this format will result in error. Upon hc.bmp being modified, the guess will be written in slword.txt in the same directory." << endl;
		cout << endl;
		cout << "FOCR Resurrection repository: https://gitlab.com/idlersc/focr-resurrection" << endl;
		return 1;
	}

	loadDictionary("data/wrds.txt");
	loadModel("data/model.txt");
	
	//user wants us to convert a bitmap
	if(strstr(argv[1], ".bmp") != NULL) {
		getString(argv[1], result);
		cout <<  result << endl;
	} else {
		//user specified a directory
		char hc[1024];
		char slword[1024];
		char timeBuffer[128];
		struct stat tmp;
		time_t previousTimestamp = 0, currentTimestamp = 0;

		memset(hc, 0, sizeof(hc));
		strncpy(hc, argv[1], sizeof(hc));
		strncat(hc, "/hc.bmp", sizeof(hc));

		memset(slword, 0, sizeof(slword));
		strncpy(slword, argv[1], sizeof(slword));
		strncat(slword, "/slword.txt", sizeof(slword));


		cout << "Waiting for " << hc << " to be modified by bot..." << endl;

		while(true) {
			stat(hc, &tmp);

			currentTimestamp = tmp.st_mtim.tv_sec;

			if(currentTimestamp != previousTimestamp) {
				//file changed.
				previousTimestamp = currentTimestamp;
				getString(hc, result);

				getTime(timeBuffer, sizeof(timeBuffer));
				cout << "[" << timeBuffer << "] guess: " << result << endl;

				if(writeToFile(slword, result) < 0) {
					cout << "Error writing guess to " << slword << endl;
					return 1;
				}
			}

			sleep(1);
		}
	}




	return 0;
}
