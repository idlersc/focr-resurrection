CC = g++
CFLAGS = -Wall

default: focr

focr: DictSearch.o EditDistance.o ImageClasifier.o Image.o ImageTransform.o Line.o main.o Segmentor.o svm.o 
	$(CC) $(CFLAGS) -o focr DictSearch.o EditDistance.o ImageClasifier.o Image.o ImageTransform.o Line.o main.o Segmentor.o svm.o

DictSearch.o: DictSearch.cpp DictSearch.h
	$(CC) $(CFLAGS) -c DictSearch.cpp

EditDistance.o: EditDistance.cpp EditDistance.h
	$(CC) $(CFLAGS) -c EditDistance.cpp

ImageClasifier.o: ImageClasifier.cpp ImageClasifier.h
	$(CC) $(CFLAGS) -c ImageClasifier.cpp

Image.o: Image.cpp Image.h errordefns.h
	$(CC) $(CFLAGS) -c Image.cpp


ImageTransform.o: ImageTransform.cpp ImageTransform.h 
	$(CC) $(CFLAGS) -c ImageTransform.cpp

main.o: main.cpp DictSearch.h ImageClasifier.h Segmentor.h errordefns.h
	$(CC) $(CFLAGS) -c main.cpp

Segmentor.o: Segmentor.cpp Segmentor.h
	$(CC) $(CFLAGS) -c Segmentor.cpp

svm.o: svm.cpp svm.h
	$(CC) $(CFLAGS) -c svm.cpp


clean:
	rm focr *.o
